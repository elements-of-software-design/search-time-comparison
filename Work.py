#  File: Work.py

#  Description: Uses a binary and linear search to evaluate
#  minimum productivity then compares time taken for each search

#  Student Name: Christopher Calizzi

#  Student UT EID: CSC3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 2/21/20

#  Date Last Modified: 2/21/20

import time

# Input: int n, the number of lines of code to write
#        int k, the productivity factor
# Output: the number of lines of code that must be
#         written before the first cup of coffee
def linear_search(n: int, k: int) -> int:
    # use linear search here
    v = 0
    while lines_written(v,k)<n:
        v +=1
    return v # placeholder

# Input: int n, the number of lines of code to write
#        int k, the productivity factor
# Output: the number of lines of code that must be
#         written before the first cup of coffee
def binary_search (n: int, k: int) -> int:
    # use binary search here
    lo = 0
    hi = n
    mid = (hi+lo)//2
    while lo<=hi:
        if lines_written(mid,k)<n:
            lo = mid+1
            mid = (hi+lo)//2
        elif lines_written(mid,k)>n:
            hi = mid-1
            mid = (hi+lo)//2
        else:
            return mid
    return mid + 1

def lines_written(v,k):
    if v>0:
        return v + lines_written(v//k,k)
    else:
        return 0
# main has been completed for you
# do NOT change anything below this line
def main():
  in_file = open("work.txt", "r")
  num_cases = int((in_file.readline()).strip())

  for i in range(num_cases):
    inp = (in_file.readline()).split()
    n = int(inp[0])
    k = int(inp[1])

    start = time.time()
    print("Binary Search: " + str(binary_search(n, k)))
    finish = time.time()
    print("Time: " + str(finish - start))

    print()

    start = time.time()
    print("Linear Search: " + str(linear_search(n, k)))
    finish = time.time()
    print("Time: " + str(finish - start))

    print()
    print()

# The line above main is for grading purposes only.
# DO NOT REMOVE THE LINE ABOVE MAIN
if __name__ == "__main__":
  main()
